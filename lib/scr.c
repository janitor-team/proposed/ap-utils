/*
 *      scr.c from Access Point SNMP Utils for Linux
 *	program output & screen related functions
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ap-utils.h"
#include <menu.h>

extern WINDOW *main_sub, *win_for_help, *main_win;
extern short ap_type, ap_vendorext;

enum { dBm, percentage, rawval } sts_viewtype = dBm;

void
scroll_rows(struct MacListStat *first, int begin, int end, int row,
	    int stat)
{
    int i = 1;
    struct MacListStat *curr = first;
    char message[100];

    clear_main(row);

    while (i++ < begin)
	curr = curr->next;
    i = 0;
    while (end-- > begin) {
	if (stat == 1) {
	    /* NWN STAtions listing */
	    sprintf(message,
		"%4u       %02X%02X%02X%02X%02X%02X        %3u%5u     %3d",
		begin + i, curr->addr[0] & 0xFF, curr->addr[1] & 0xFF,
		curr->addr[2] & 0xFF, curr->addr[3] & 0xFF,
		curr->addr[4] & 0xFF, curr->addr[5] & 0xFF,
		curr->quality,
		curr->idle,
		conv_rssi(curr->rssi)
	    );
	} else if (stat == 2) {
	    /* ATMEL STAtions listing */
	    char parentmac[13] = "      -     ";
	    char rssi[5] = "   -";
	    char quality[5] = "   -";
	    char status[4] = "  -";
	    char port[4] = " -";
	    char ip[16] = "      -        ";

	    if (curr->ParentMacAddress[0] | curr->ParentMacAddress[1] |
		curr->ParentMacAddress[2] | curr->ParentMacAddress[3] |
		curr->ParentMacAddress[4] | curr->ParentMacAddress[5])
		sprintf(parentmac, "%02X%02X%02X%02X%02X%02X",
		    curr->ParentMacAddress[0] & 0xFF,
		    curr->ParentMacAddress[1] & 0xFF,
		    curr->ParentMacAddress[2] & 0xFF,
		    curr->ParentMacAddress[3] & 0xFF,
		    curr->ParentMacAddress[4] & 0xFF,
		    curr->ParentMacAddress[5] & 0xFF);

	    if (curr->rssi)
		sprintf(rssi, "%4d", conv_rssi(curr->rssi));

	    if (curr->quality)
		sprintf(quality, "%3u%%", curr->quality);

	    if (curr->Status)
		sprintf(status, "%3u", curr->Status);

	    if (curr->Port)
		sprintf(port, "%2u", curr->Port);

	    if(curr->IP.s_addr)
		sprintf(ip, "%s", inet_ntoa(curr->IP));

	    sprintf(message, "%3u %02X%02X%02X%02X%02X%02X %s %s %s %s   %s %s",
		begin + i,
		curr->addr[0] & 0xFF, curr->addr[1] & 0xFF,
		curr->addr[2] & 0xFF, curr->addr[3] & 0xFF,
		curr->addr[4] & 0xFF, curr->addr[5] & 0xFF,
		parentmac, rssi, quality, status, port, ip
	    );
	} else {
	    /* MAC auth table listing */
	    sprintf(message, " %3u     %02X%02X%02X%02X%02X%02X",
		begin + i, curr->addr[0] & 0xFF, curr->addr[1] & 0xFF,
		curr->addr[2] & 0xFF, curr->addr[3] & 0xFF,
		curr->addr[4] & 0xFF, curr->addr[5] & 0xFF
	    );
	}
	mvwaddstr(main_sub, row + i, 0, message);
	i++;
	curr = curr->next;
    }
    wrefresh(main_sub);
}

void print_help(char *mess)
{
    unsigned short int j, len = (COLS - strlen(mess))/2;

    werase(win_for_help);

    for (j = 0; j <= len; j++) {
	mvwaddch(win_for_help, 0, j, ' ');
	mvwaddch(win_for_help, 0, COLS - j - 1, ' ');
    }
    
    mvwaddstr(win_for_help, 0, len, mess);
    wrefresh(win_for_help);
}

void print_helperr(char *mess)
{
    wattrset(win_for_help, COLOR_PAIR(14) | A_BOLD);
    print_help(mess);
    wattrset(win_for_help, COLOR_PAIR(11));
}

void print_bold(WINDOW *wptr, char *mess)
{
    wattrset(wptr, A_BOLD);
    waddstr(wptr, mess);
    wattrset(wptr, A_NORMAL);
    wrefresh(wptr);
}

void print_menusel(int y, int x, char *mess1, char *mess2)
{
    mvwaddstr(main_sub, y, x, mess1);
    print_bold(main_sub, mess2);
}

void print_bottom(char *mess)
{
    int i;
    char message[100];
    extern short ap_type, ap_vendorext;
    extern char *ap_types[], *ap_vendorexts[][3];

    sprintf(message, _("Current AP: %s Type: %s Ext: %s"),
	mess, ap_types[ap_type], ap_vendorexts[ap_type][ap_vendorext]);
    /* ����� ����� */
    for (i = 0; i < COLS - MCOLS - 1; i++)
	mvwaddch(main_win, LINES - 3, i, ACS_BSBS);

    wmove(main_win, LINES - 3, COLS - MCOLS - strlen(message) - 1);
    print_bold(main_win, message);
}

void print_top(char *title_left, char *title_right)
{
    int i = 0;

    wmove(main_win, 0, 0);
    while (i++ < COLS - MCOLS - 1)
	waddch(main_win, ACS_BSBS);

    wattrset(main_win, A_BOLD);
    if (title_left)
	mvwaddstr(main_win, 0, 2, title_left);

    if (title_right)
	mvwaddstr(main_win, 0, COLS - MCOLS - strlen(title_right) - 2,
	    title_right);

    wattrset(main_win, A_NORMAL);
    wrefresh(main_win);
}

void print_top_rssi(char *message_r)
{
    char message_l[200];

    sprintf(message_l, "%s", "RSSI: ");
    switch (sts_viewtype) {
	case dBm:
	   strcat(message_l, "dBm");
	   break;
	case percentage:
	   strcat(message_l, "%");
	   break;
	case rawval:
	   strcat(message_l, "raw");
    }
    print_top(message_l, message_r);
}

void clear_main(int m)
{
    clear_main_new(m, LINES - 4);
}

void clear_main_new(int m, int n)
{
    int i, j;
    wmove(main_sub, m, 0);
    for (i = m; i < n; i++)
	for (j = 0; j < COLS - MCOLS - 1; j++)
	    waddch(main_sub, ' ');
    wrefresh(main_sub);
}

int conv_rssi(int raw_rssi)
{
    switch (sts_viewtype) {
	case dBm:
	   return (dbmconv(raw_rssi)); 
	case percentage:
	   return ((int)((minimum (raw_rssi, 40)) * (float)2.5));
	case rawval:
	   return raw_rssi;
    }
    /* Should never happen */
    return -1;
}

