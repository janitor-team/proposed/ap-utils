/*
 *    ap-utils.h from Wireless Access Point Utilites for Unix
 *  Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef	_AP_CNF
#define _AP_CNF 1

#include "config.h"

#ifdef OS_BSD
#include <sys/types.h>
#ifdef OS_X
#include <stdint.h>
#endif /* ifdef OS_X */
#else
#ifdef OS_SOLARIS
#include <sys/types.h>
#else
#include <features.h>
#if defined (__GLIBC__)
#include <stdint.h>
#else
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
#endif /* if defined (__GLIBC__) */
#endif /* ifdef OS_SOLARIS */
#endif /* ifdef OS_BSD */

#include "ap-curses.h"

#define TITLE "Wireless Access Point Utilites for Unix"
#define VIEW "View: "

#define minimum(x, y)	x <= y ? x : y

#ifdef HAVE_GETTEXT
/* GNU gettext stuff*/
#include <locale.h>
#include <libgnuintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#ifdef WORDS_BIGENDIAN
/*
 * a quick-and-dirty macros to unconditionally swap bytes in 2 and 4-byte
 * integers between big-endian and little-endian ordering
 */
#define swap2(d) ((((d) >> 8) & 0x00ff) | (((d) << 8) & 0xff00))
#define swap4(d) ((((d) >> 24) & 0x000000ff) | (((d) >>  8) & 0x0000ff00) | (((d) <<  8) & 0x00ff0000) | (((d) << 24) & 0xff000000))
#else
#define swap2(d) d
#define swap4(d) d
#endif

#define rlong(a)	((a) = swap4(a))
#define wlong(a)	((a) = swap4(a))
#define rshort(a)	((a) = swap2(a))
#define wshort(a)	((a) = swap2(a))

#define dbmconv(x)	(ap_type == ATMEL410 && ap_vendorext == SBRIDGES) ? \
			    (int)(-95 + x * (float)2.125) : -96 + x

#define MAC _("MAC address: ")
#define TRAPS _("[S] SNMP traps: ")

#define CHANNEL _("[C] Frequency channel: ")

#define ANTENNA_RX _("Receive  antenna:")
#define ANTENNA_RX_LEFT  _("[U] Left")
#define ANTENNA_RX_RIGHT _("[I] Right")
#define ANTENNA_TX _("Transmit antenna:")
#define ANTENNA_TX_LEFT  _("[O] Left")
#define ANTENNA_TX_RIGHT _("[P] Right")
#define ANTENNA_DV _("Diversity select:")
#define ANTENNA_DV_LEFT  _("[T] Left")
#define ANTENNA_DV_RIGHT _("[Y] Right")

#define YES _("Yes")
#define NO _("No")

#define ON _("On")
#define OFF _("Off")

#define BASIC _("Basic")

#define ANY_KEY _("Press any key to continue.")
#define QT_HELP _("Q - quit to menu. T - toggle polling mode, Other key - force update")

#define ERR_SET _("Unable to write data to AP. Press any key to continue.")
#define ERR_RET _("Unable to retrieve (valid) data from AP. Press any key to continue.")
#define WAIT_RET _("Trying to retrieve data from AP - please wait (or press Q to quit).")
#define WAIT_SET _("Writing data to AP. Please wait...")
#define DONE_SET _("Configuration written to the AP. Press any key to continue.")
#define ERR_SELECT _("select() function error. Press any key.")
#define ERR_SOCKET _("socket() or bind() function error. Press any key.")

#define ERR_WRITING_APCONF _("Unable to write AP list file ~/.ap-config. Press any key.")
#define DONE_WRITING_APCONF _("AP list file ~/.ap-config successfully written. Press any key.")

#define MAIN_MENU _("Back to main menu")
#define MENU_EXIT _("Exit program")
#define MENU_SHELL _("Run subshell. To return type 'exit'.")
#define MENU_POLLING _("Change polling mode interval")
#define MENU_ABOUT _("Short info about program")
#define MENU_SEARCH _("Find connected Access Points")
#define MENU_CONNECT _("Set connection options: ip and community")
#define MENU_ENCRYPT _("Set encryption; edit WEP keys")
#define MENU_AUTH _("Set authorization; edit MAC authorization table")
#define MENU_COMMUNITY _("Set SNMP community/password for access to the AP")
#define MENU_SYSINFO _("Get info about AP hardware and firmware")
#define MENU_WIRELESS _("Get wireless port statistics")
#define MENU_STAS _("Get list of currently associated stations (Access Point clients)")
#define MENU_APLINK _("Get link status in APclient mode")
#define MENU_INFO _("Get info and statistics from AP")
#define MENU_CONFIG _("Set various configuration options")

#define ST_TITLE _("Associated stations")
#define AP_TITLE _("AP Client link state")

#define POLL_ON _("Polling: on")
#define POLL_OFF _("Polling: off")

#define INT_VALUE	0x02
#define STRING_VALUE	0x04
#define NULL_VALUE	0x05
#define OID_VALUE	0x06

#define GET             0xA0
#define GET_NEXT        0xA1
#define RESPONSE        0xA2
#define SET             0xA3
#define TRAP            0xA4

#define ASN_HEADER	0x30

#define INT_STRING	0x01
#define HEX_STRING	0x02
#define ANY_STRING	0x03

#define SIZE sizeof(struct sockaddr_in)
#define basic(i) (i & 0x80) ? BASIC : (i) ? ON : OFF


#define MCOLS 15 /* width of menu window */
#define LAST_ROW LINES-5

#define WAIT_TIMEOUT 1
#define WAIT_FOREVER 0

/* Basic distinguished MIB (and AP) types */
/* Numbering with respect to order in ap_types */
#define ATMEL410 0
#define NWN 1
#define ATMEL12350 2

/* Vendor distinguished MIB subtypes */
/* Numbering with respect to order in ap_vendorexts */
#define NONE 0
#define SBRIDGES 1
#define GEMTEK 1
#define EZYNET 2

typedef struct {
    unsigned char *oid;
    int len_oid;
    unsigned char *value;
    int len_val;
    unsigned char type;
} varbind;

typedef struct {
    char code;
    char *desc;
    unsigned short first_ch;
    unsigned short chans;
} rdprops;

struct umitems {
	char *item;
	char *help;
	void (*func)();
	int is_menu;
};

struct EthRxStatistics_s {
	uint32_t TotalBytesRx;
	uint32_t TotalPacketsRx;
	uint32_t PacketCRCErrorRx;
	uint32_t MulticastPacketRx;
	uint32_t BroadcastPacketRx;
	uint32_t ControlFramesRx;
	uint32_t PauseFramesRx;
	uint32_t UnknownOPCodeRx;
	uint32_t AlignmentRxError;
	uint32_t LengthOutOfRangeRx;
	uint32_t CodeErrorRx;
	uint32_t FalseCarrierRx;
	uint32_t UndersizePacketsRx;
	uint32_t OversizePacketsRx;
	uint32_t TotalFragmentsRx;
	uint32_t TotalJabberRx;
};

struct EthTxStatistics_s {
	uint32_t TotalBytesTx;
	uint32_t TotalPacketsTx;
	uint32_t PacketCRCErrorTx;
	uint32_t MulticastPacketTx;
	uint32_t BroadcastPacketTx;
	uint32_t UnicastPacketTx;
	uint32_t PauseFramesTx;
	uint32_t SingleDeferPacketTx;
	uint32_t MultiDeferPacketsTx;
	uint32_t SingleCollisionsTx;
	uint32_t MultiCollisionsTx;
	uint32_t LateCollisionsTx;
	uint32_t ExcessiveCollisionTx;
	uint32_t TotalCollisionsTx;
};

struct wirelessStatistics_s {
	uint32_t UnicastTransmittedPackets;
	uint32_t BroadcastTransmittedPackets;
	uint32_t MulticastTransmittedPackets;
	uint32_t TransmittedBeacon;
	uint32_t TransmittedACK;
	uint32_t TransmittedRTS;
	uint32_t TransmittedCTS;
	uint32_t UnicastReceivedPackets;
	uint32_t BroadcastReceivedPackets;
	uint32_t MulticastReceivedPackets;
	uint32_t ReceivedBeacon;
	uint32_t ReceivedACK;
	uint32_t ReceivedRTS;
	uint32_t ReceivedCTS;
	uint32_t ACKFailure;
	uint32_t CTSFailure;
	uint32_t RetryPackets;
	uint32_t ReceivedDuplicate;
	uint32_t FailedPackets;
	uint32_t AgedPackets;
	uint32_t FCSError;
	uint32_t InvalidPLCP;
	/* following 4 are specific for ATMEL 12350 MIB enhanced by EZYNET */
	uint32_t TransmittedPackets_11Mbps;
	uint32_t TransmittedPackets_55Mbps;
	uint32_t TransmittedPackets_2Mbps;
	uint32_t TransmittedPackets_1Mbps;
};

struct sysDeviceInfo_128 {
    /* sysDeviceInfo OID struct - version 128 bytes long */
    uint32_t StructVersion;
    unsigned char MacAddress[6];
    unsigned short Reserved;
    uint32_t RegulatoryDomain;
    uint32_t ProductType;
    unsigned char OEMName[32];
    uint32_t OEMID;
    unsigned char ProductName[32];
    uint32_t HardwareRevision;
    /*
     * NOTE: Although MIBs claim 92 bytes length, it does not need to be true.
     * 92 bytes seems to be just the 'official' part; the unofficial
     * (and undocumented) remnant padding the total length to 128 bytes
     * may or may not eventually appear (resulting in returned length 128 b.).
     */
};

struct sysDeviceInfo_160 {
    /* sysDeviceInfo OID struct - version 160 bytes long */
    uint32_t StructVersion;
    unsigned char MacAddress[6];
    unsigned char Channel;
    unsigned char RegulatoryDomain;
    uint32_t ProductType;
    unsigned char OEMName[32];
    uint32_t OEMID;
    unsigned char ProductName[32];
    uint32_t HardwareRevision;
    /* The rest is mostly dormant. */
    unsigned char PID_VID[4];
    uint32_t sysOIDSize;
    uint16_t sysOID[16];
    unsigned char CountryCode[3];
    unsigned char Reserved1;
    uint16_t ChannelInformation;
    unsigned char Reserved2[2];
    unsigned char TxPower[14];
    unsigned char Reserved3[10];
};

/* AP mode: Associated stations diagnostics */

struct AssociatedSTAsInfo_ATMEL410 {
    unsigned short Index;
    unsigned char  MacAddress[6];
    /* The rest is only supported by ATMEL410 SBRIDGES MIB */
    unsigned char  Status;
    unsigned char  Port;
    unsigned char  ParentMacAddress[6];
    unsigned char  RSSI;
    unsigned char  LinkQuality;
    unsigned char  IP[4];
    unsigned char  Reserved1[2];
};

struct AssociatedSTAsInfo_ATMEL12350 {
    unsigned short Index;
    unsigned char  MacAddress[6];
    /* The rest is only supported by ATMEL12350 EZYNET MIB */
    unsigned char  Status;
    unsigned char  Port;
    unsigned char  ParentMacAddress[6];
    unsigned char  RSSI;
    unsigned char  IP[4];
    unsigned char  Reserved1[3];
};

/* This structure unifies all the unique members of the 2 structs above */
struct MacListStat {
    unsigned char addr[6];
    struct MacListStat *next;
    int quality;
    int idle;
    int rssi;
    unsigned char  Status;
    unsigned char  Port;
    unsigned char  ParentMacAddress[6];
    struct in_addr IP;
};

/* APClient mode: AP link diagnostics */

/* size: 53 bytes */
struct NetworkSettings_ATMEL410_SBRIDGES {
    unsigned short reserved1;
    unsigned char BSSID[6];
    unsigned short InfoCapability;
    unsigned char Rssi;
    unsigned char Channel;
    unsigned char reserved2[2];
    unsigned char LinkQuality;
    unsigned char reserved3[5];
    unsigned char ESSID[32];
    unsigned char ESSLEN;
};

/* size: 56 bytes */
struct NetworkSettings_ATMEL12350_EZYNET {
    unsigned short reserved1;
    unsigned char BSSID[6];
    unsigned short InfoCapability;
    unsigned char Rssi;
    unsigned char Channel;
    unsigned char reserved2[2];
    unsigned char CurrentRate;
    unsigned char reserved3[5];
    unsigned char ESSID[32];
    unsigned long ESSLEN;
};

/* function prototypes specific/common for both utilites */

/* service */
extern char * oui2manufacturer(char *);
extern int  regdom_idx(char);
extern int  ch_list(int, char **);
extern void connect_options(unsigned long int, int);
extern int  get_mib_details(void);
extern void about(void);
extern void draw_menu_win(void);
extern void main_menu(void);
extern void print_bold(WINDOW *, char *);
extern void print_menusel(int, int, char *, char *);
extern void print_bottom(char *);
extern void print_top(char *, char *);
extern void print_top_rssi(char *);
extern void print_help(char *);
extern void print_helperr(char *);
extern void clear_main(int);
extern void clear_main_new(int, int);
extern int  conv_rssi(int);
extern void get_mac(char *, int, int);
extern void get_ip(struct in_addr *, int, int, char *);
extern void get_mask(struct in_addr *, int, int, char *);
extern void get_value(char *, int, int, int, char, unsigned int, unsigned int,
		      char *);
extern void get_pass(char *, int, int, int);
extern int  yes_no(int, int);
extern int  on_off(int, int);
extern int  menu_choose(int, int, char **, unsigned int);
extern int  wait_key(int);
extern int  help_ysn(void);
extern void scroll_rows(struct MacListStat *, int, int, int, int);
extern void exit_shell(void);
extern void exit_program(void);
extern void polling_interval(void);
extern int  get_opts(void);
extern void save_Stations(struct MacListStat *);
extern void uni_menu(struct umitems *umenu, int num);

/* AP */
extern void AuthorizedSettings(void);
extern void bridging(void);
extern void wep(void);
extern void WirelessStat(void);
extern void atmel_stations(void);
extern void atmel_aplink(void);
extern void atmel_sysinfo(void);
extern void atmel_set_oeminfo(void);
extern void ap_search(void);

/* snmp engine*/
extern void close_sockfd(void);
extern int  open_sockfd(void);
extern int  reopen_sockfd(void);
extern int snmp(varbind *, int, int);
extern int ber(char *, varbind *, int, int);
/* extern unsigned int ber_decode_uint(unsigned char *, int); */

/* function prototypes specific for ATMEL utility */
extern void reset(void);
extern void defaults(void);
extern void upload(void);
extern int  SysUpload(void);
extern int  SysReset(void);
extern void EthStat(void);
extern void APs(void);
extern void power(void);
extern void test(void);
extern void atmel_wireless(void);
extern void atmel_auth(void);

/* function prototypes specific for NWN utility */
extern void latest(void);
extern int  get_RegDomain(void);
extern void advanced(void);
extern void command_menu(void);
extern void nwn_sysinfo(void);
extern void nwn_wep(void);
extern void nwn_stations(void);
extern void nwn_wireless_stat(void);
extern void nwn_wireless(void);
extern void nwn_auth_mac(void);

#endif
