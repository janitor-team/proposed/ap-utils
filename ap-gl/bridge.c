/*
 *      bridge.c from Access Point SNMP Utils for Linux
 *
 * Copyright (c) 2002 Roman Festchook <roma at polesye dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2 from
 * June 1991 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "ap-utils.h"

#define IPADDR _("[I] IP: ")
#define NETMASK _("[N] Netmask: ")
#define GATEWAY _("[G] Gateway: ")
#define IP_FILTER _("[F] Filter non-IP traffic: ")
#define PR_PORT _("[P] Primary port: ")
#define SB_ATTMAC _("Attached station MAC: ")
#define DHCP _("[D] DHCP client: ")
#define OPER _("[O] Operational mode: ")
#define REMOTE_MAC _("[M] Preferred BSSID (remote MAC addr.): ")
//#define CF_PORT _("[C] Configuration-enabled port(s): ")
#define TRAP_PORT _("[T] Trap-sending port: ")
#define FW_BCAST _("[R] Forward broadcast traffic: ")
//#define SB_BCAST _("[B] Isolate wireless clients (broadcast traffic): ")
#define SB_UCAST _("[U] Isolate wireless clients: ")
#define HELP _("INGFPDOMSCTRBU - set; W - write conf; Q - quit to menu")

extern short ap_type;


void bridging()
{

char *bridge_modes[6] = {
    _("Wireless Bridge Point to MultiPoint"),
    _("Access Point"),
    _("Access Point client"),
    _("Wireless Bridge Point to Point"),
    _("Repeater"),
    _("unknown")
};


    char sysTrapSwitch[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x01, 0x03,
	0x00
    };

    char operIPAddress[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x01,
	0x00
    };
    char operIPMask[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x02,
	0x00
    };
    char operEthernetAddress[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x03,
	0x00
    };
    char operGateway[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x04,
	0x00
    };
    char operDHCP[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x05,
	0x00
    };
    char PrimaryPort[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x02, 0x06,
	0x00
    };
/*    char ConfigPort[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0xE0, 0x3E, 0x01, 0x01, 0x02, 0x07,
	0x00
    };
*/
    char TrapPort[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0xE0, 0x3E, 0x01, 0x01, 0x02, 0x08,
	0x00
    };

    char IPFilter[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x03, 0x01,
	0x00
    };

    char ForwarbBcast[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x03, 0x02,
	0x00
    };
    char SendBackBcast[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x03, 0x03,
	0x00
    };
    char SendBackUnicast[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x03, 0x04,
	0x00
    };

    char bridgeOperationalMode[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x04, 0x01,
	0x00
    };
    char bridgeRemoteBridgeBSSID[] =
	{ 0x2B, 0x06, 0x01, 0x04, 0x01, 0x83, 0x1A, 0x01, 0x01, 0x04, 0x02,
	0x00
    };

    extern WINDOW *main_sub;
    varbind varbinds[15];
    struct in_addr ip, mask, gw;
    unsigned char message[1024], filter, primary_port, dhcp, RemoteBSSID[6],
	bridge_mode, traps, fw_bcast, sb_ucast;
    char m_filter = 0, m_bridge_mode = 0, m_primary_port = 0, m_traps = 0,
	m_dhcp = 0, m_ip = 0, m_mask = 0, m_gw = 0, m_remote_bssid = 0,
	/*m_config_port = 0, */m_trap_port = 0, m_fw_bcast = 0, /*m_sb_bcast = 0,*/
	m_sb_ucast = 0;
    char *pr_ports[2] = {
	_("Ethernet"),
	_("Wireless")
    } /*, *cf_trap_ports[3] = {
	_("Ethernet"),
	_("Wireless"),
	_("Both")
    }*/;

    int i;
    unsigned int trap_port = 0;

    /* Determine the advanced MIB subtype of the device from its MAC address. */
    varbinds[0].type = NULL_VALUE;
    varbinds[0].oid = operEthernetAddress;
    varbinds[0].len_oid = sizeof(operEthernetAddress);
    varbinds[0].len_val = 0;

    print_help(WAIT_RET);
/*    if (snmp(varbinds, 1, GET) <= 0) {
	print_helperr(ERR_RET);
	goto exit;
    }
*/
    for (i = 1; i < 15; i++) {
	varbinds[i].type = NULL_VALUE;
	varbinds[i].len_val = 0;
	varbinds[i].len_oid = sizeof(sysTrapSwitch);
    }

    varbinds[1].oid = IPFilter;
    varbinds[2].oid = PrimaryPort;
    varbinds[3].oid = operDHCP;
    varbinds[4].oid = operIPAddress;
    varbinds[5].oid = operIPMask;
    varbinds[6].oid = operGateway;
    varbinds[7].oid = bridgeOperationalMode;
    varbinds[8].oid = sysTrapSwitch;
    varbinds[9].oid = bridgeRemoteBridgeBSSID;
    varbinds[10].oid = ForwarbBcast;
    varbinds[11].oid = SendBackUnicast;
    varbinds[12].oid = SendBackBcast;

    if (snmp(varbinds, 13, GET) < 13) {
	print_helperr(ERR_RET);
	goto exit;
    }

    print_top(NULL, _("Bridging"));

    sb_ucast = *(varbinds[11].value);
//    sb_ucast = varbinds[11].len_val;
    sprintf(message, "%s%s", SB_UCAST, (sb_ucast == 2) ? ON : OFF);
    mvwaddstr(main_sub, 10, 0, message);



    sprintf(message, "%s%02X%02X%02X%02X%02X%02X", MAC,
	    varbinds[0].value[0] & 0xFF,
	    varbinds[0].value[1] & 0xFF,
	    varbinds[0].value[2] & 0xFF,
	    varbinds[0].value[3] & 0xFF,
	    varbinds[0].value[4] & 0xFF,
	    varbinds[0].value[5] & 0xFF);
    mvwaddstr(main_sub, 0, 0, message);

    filter = *(varbinds[1].value);
    sprintf(message, "%s%s", IP_FILTER, (filter == 1) ? ON : OFF);
    mvwaddstr(main_sub, 3, 0, message);

    primary_port = *(varbinds[2].value);
	if (primary_port < 1 || primary_port > 2) {
	    primary_port = 1;
	}
	sprintf(message, "%s%s", PR_PORT, pr_ports[primary_port - 1]);
    mvwaddstr(main_sub, 4, 0, message);

    dhcp = *(varbinds[3].value);
    sprintf(message, "%s%s", DHCP, (dhcp == 1) ? ON : OFF);
    mvwaddstr(main_sub, 5, 0, message);

    memcpy(&ip.s_addr, varbinds[4].value, 4);
    sprintf(message, "%s%s", IPADDR, inet_ntoa(ip));
    mvwaddstr(main_sub, 1, 0, message);

    memcpy(&mask.s_addr, varbinds[5].value, 4);
    sprintf(message, "%s%s", NETMASK, inet_ntoa(mask));
    mvwaddstr(main_sub, 1, 24, message);

    memcpy(&gw.s_addr, varbinds[6].value, 4);
    sprintf(message, "%s%s", GATEWAY, inet_ntoa(gw));
    mvwaddstr(main_sub, 2, 0, message);

    memcpy(RemoteBSSID, varbinds[9].value, 6);

    if ((bridge_mode = *(varbinds[7].value)) != 2) {
	sprintf(message, "%s%02X%02X%02X%02X%02X%02X", REMOTE_MAC,
		*(RemoteBSSID + 0) & 0xFF, *(RemoteBSSID + 1) & 0xFF,
		*(RemoteBSSID + 2) & 0xFF, *(RemoteBSSID + 3) & 0xFF,
		*(RemoteBSSID + 4) & 0xFF, *(RemoteBSSID + 5) & 0xFF);
	mvwaddstr(main_sub, 7, 0, message);
    }
    if (bridge_mode > 5)
	bridge_mode  = 6;
    sprintf(message, "%s%s", OPER, bridge_modes[bridge_mode - 1]);
    mvwaddstr(main_sub, 6, 0, message);

    traps = *(varbinds[8].value);
    sprintf(message, "%s%s", TRAPS, (traps == 1) ? ON : OFF);
    mvwaddstr(main_sub, 8, 0, message);

    fw_bcast = *(varbinds[10].value);
    sprintf(message, "%s%s", FW_BCAST, (fw_bcast == 1) ? ON : OFF);
    mvwaddstr(main_sub, 9, 0, message);

    wrefresh(main_sub);
    noecho();

    print_help(HELP);
    while (1) {
	switch (getch()) {
	case 'I':
	case 'i':
	    get_ip(&ip, 1, strlen(IPADDR), HELP);
	    m_ip = 1;
	    continue;
	case 'N':
	case 'n':
	    get_mask(&mask, 1, strlen(IPADDR) + 16 + strlen(NETMASK), HELP);
	    m_mask = 1;
	    continue;
	case 'G':
	case 'g':
	    get_ip(&gw, 2, strlen(GATEWAY), HELP);
	    m_gw = 1;
	    continue;
	case 'F':
	case 'f':
	    filter = on_off(3, strlen(IP_FILTER));
	    clear_main_new(3, 4);
	    print_menusel(3, 0, IP_FILTER, (filter == 1) ? ON : OFF);
	    m_filter = 1;
	    continue;
	case 'P':
	case 'p':
		primary_port = menu_choose(4, strlen(PR_PORT), pr_ports, 2) + 1;
		clear_main_new(4, 5);
		print_menusel(4, 0, PR_PORT, pr_ports[primary_port - 1]);
		m_primary_port = 1;
	    continue;
	case 'D':
	case 'd':
	    dhcp = on_off(5, strlen(DHCP));
	    clear_main_new(5, 6);
	    print_menusel(5, 0, DHCP, (dhcp == 1) ? ON : OFF);
	    m_dhcp = 1;
	    continue;
	case 'O':
	case 'o':
	    bridge_mode = menu_choose(6, strlen(OPER), bridge_modes, 5) + 1;
	    clear_main_new(6, 8);
	    print_menusel(6, 0, OPER, bridge_modes[bridge_mode - 1]);
	    if (bridge_mode != 2) {
		sprintf(message, "%02X%02X%02X%02X%02X%02X",
			*(RemoteBSSID + 0) & 0xFF,
			*(RemoteBSSID + 1) & 0xFF,
			*(RemoteBSSID + 2) & 0xFF,
			*(RemoteBSSID + 3) & 0xFF,
			*(RemoteBSSID + 4) & 0xFF,
			*(RemoteBSSID + 5) & 0xFF);
		print_menusel(7, 0, REMOTE_MAC, message);
	    }
	    m_bridge_mode = 1;
	    continue;
	case 'M':
	case 'm':
	    if (bridge_mode == 2)
		continue;
	    get_mac(RemoteBSSID, 7, strlen(REMOTE_MAC));
	    m_remote_bssid = 1;
	    continue;
	case 'S':
	case 's':
	    traps = on_off(8, strlen(TRAPS));
	    clear_main_new(8, 9);
	    print_menusel(8, 0, TRAPS, (traps == 1) ? ON : OFF);
	    m_traps = 1;
	    continue;
/*	case 'C':
	case 'c':
		config_port = menu_choose(9, strlen(CF_PORT), cf_trap_ports, 3);
		clear_main_new(9, 10);
		print_menusel(9, 0, CF_PORT, cf_trap_ports[config_port]);
		m_config_port = 1;
	    continue;
*/	case 'R':
	case 'r':
		fw_bcast = on_off(9, strlen(FW_BCAST));
		clear_main_new(9, 10);
		print_menusel(9, 0, FW_BCAST, (fw_bcast == 1) ? ON : OFF);
		m_fw_bcast = 1;
	    continue;
/*	case 'B':
	case 'b':
		sb_bcast = on_off(12, strlen(SB_BCAST));
		clear_main_new(12, 13);
		print_menusel(12, 0, SB_BCAST, (sb_bcast == 1) ? ON : OFF);
		m_sb_bcast = 1;
	    continue;
*/	case 'U':
	case 'u':
		sb_ucast = on_off(10, strlen(SB_UCAST));
		sb_ucast = (sb_ucast == 2) ? 1 : 2;
		clear_main_new(10, 11);
		print_menusel(10, 0, SB_UCAST, (sb_ucast == 2) ? ON : OFF);
		m_sb_ucast = 1;
	    continue;
	case 'w':
	case 'W':
	    i = 0;
	    if (m_filter) {
		varbinds[i].oid = IPFilter;
		varbinds[i].len_oid = sizeof(IPFilter);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &filter;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_primary_port) {
		varbinds[i].oid = PrimaryPort;
		varbinds[i].len_oid = sizeof(PrimaryPort);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &primary_port;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_dhcp) {
		varbinds[i].oid = operDHCP;
		varbinds[i].len_oid = sizeof(operDHCP);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &dhcp;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_ip) {
		varbinds[i].oid = operIPAddress;
		varbinds[i].len_oid = sizeof(operIPAddress);
		ip.s_addr = htonl(ip.s_addr);
		ip.s_addr = swap4(ip.s_addr);
		varbinds[i].value = (char *) &ip.s_addr;
		varbinds[i].len_val = 4;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_mask) {
		varbinds[i].oid = operIPMask;
		varbinds[i].len_oid = sizeof(operIPMask);
		mask.s_addr = htonl(mask.s_addr);
		mask.s_addr = swap4(mask.s_addr);
		varbinds[i].value = (char *) &mask.s_addr;
		varbinds[i].len_val = 4;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_gw) {
		varbinds[i].oid = operGateway;
		gw.s_addr = htonl(gw.s_addr);
		gw.s_addr = swap4(gw.s_addr);
		varbinds[i].len_oid = sizeof(operGateway);
		varbinds[i].value = (char *) &gw.s_addr;
		varbinds[i].len_val = 4;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_traps) {
		varbinds[i].oid = sysTrapSwitch;
		varbinds[i].len_oid = sizeof(sysTrapSwitch);
		varbinds[i].value = &traps;
		varbinds[i].len_val = 1;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_bridge_mode) {
		varbinds[i].oid = bridgeOperationalMode;
		varbinds[i].len_oid = sizeof(bridgeOperationalMode);
		varbinds[i].value = (char *) &bridge_mode;
		varbinds[i].len_val = 1;
		varbinds[i].type = INT_VALUE;
		i++;
	    }
	    if (m_remote_bssid) {
		varbinds[i].oid = bridgeRemoteBridgeBSSID;
		varbinds[i].len_oid = sizeof(bridgeRemoteBridgeBSSID);
		varbinds[i].value = RemoteBSSID;
		varbinds[i].len_val = 6;
		varbinds[i].type = STRING_VALUE;
		i++;
	    }
/*	    if (m_config_port) {
		varbinds[i].oid = ConfigPort;
		varbinds[i].len_oid = sizeof(ConfigPort);
		varbinds[i].type = INT_VALUE;
		config_port +=1;
		varbinds[i].value = (char *) &config_port;
		varbinds[i].len_val = 1;
		i++;
	    }
*/	    if (m_fw_bcast) {
		varbinds[i].oid = ForwarbBcast;
		varbinds[i].len_oid = sizeof(ForwarbBcast);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &fw_bcast;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_sb_ucast) {
		varbinds[i].oid = SendBackBcast;
		varbinds[i].len_oid = sizeof(SendBackBcast);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &sb_ucast;
		varbinds[i].len_val = 1;
		i++;
		varbinds[i].oid = SendBackUnicast;
		varbinds[i].len_oid = sizeof(SendBackUnicast);
		varbinds[i].type = INT_VALUE;
		varbinds[i].value = (char *) &sb_ucast;
		varbinds[i].len_val = 1;
		i++;
	    }
	    if (m_trap_port) {
		int len_val;

		len_val = (trap_port > 0x7fff) ? 3 : (trap_port > 0x7f) ? 2 : 1;
		varbinds[i].oid = TrapPort;
		varbinds[i].len_oid = sizeof(TrapPort);
		varbinds[i].type = STRING_VALUE;
		varbinds[i].value = (char *) &trap_port;
		varbinds[i].len_val = len_val;
		i++;
	    }

	    print_help(WAIT_SET);
	    if (snmp(varbinds, i, SET) <= 0) {
		print_helperr(ERR_SET);
	    } else {
		wbkgd(main_sub, A_NORMAL);
		wrefresh(main_sub);
           	print_help(DONE_SET);
	    }
    	    goto exit;
	case 'Q':
	case 'q':
	    goto quit;
	}
	continue;
    }
  exit:
    getch();
  quit:
    print_top(NULL, NULL);
    clear_main(0);
}

